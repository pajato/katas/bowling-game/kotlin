package com.pajato.katas

fun getRolls(initialValue: Int = 0): Array<Int> = Array<Int>(21) { initialValue }

fun totalScore(rolls: Array<Int>): Int {
    var result = 0
    var index = 0
    fun isSpare(): Boolean = rolls[index] + rolls[index + 1] == 10
    fun isStrike(): Boolean = rolls[index] == 10
    fun getScoreForFrame(): Int =
        when {
            index % 2 == 0 && isStrike() -> 10 + rolls[index + 1] + rolls[index + 2]
            index % 2 == 0 && isSpare() -> 10 + rolls[index + 2]
            else -> rolls[index]
        }
    fun getNextIndex(): Int =
        when {
            isSpare() || isStrike() -> 2
            else -> 1
        }

    while (index < rolls.size - 2) {
        result += getScoreForFrame()
        index += getNextIndex()
    }
    return result
}
