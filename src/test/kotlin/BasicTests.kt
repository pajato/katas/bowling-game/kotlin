package com.pajato.katas

import org.junit.Test
import kotlin.test.assertEquals

class BasicTests {

    @Test
    fun `when all zeroes are rolled verify zero result`() {
        val rolls = getRolls()
        assertEquals(0, totalScore(rolls))
    }

    @Test
    fun `when one spare is rolled verify result`() {
        val rolls = getRolls()
        rolls[0] = 4
        rolls[1] = 6
        rolls[2] = 8
        val expected = 26
        assertEquals(expected, totalScore(rolls))
    }

    @Test
    fun `when one strike is rolled verify result`() {
        val rolls = getRolls()
        rolls[0] = 10
        rolls[1] = 6
        rolls[2] = 4
        val expected = 24
        assertEquals(expected, totalScore(rolls))
    }

    @Test
    fun `when all strikes are rolled verify result`() {
        val rolls = getRolls(10)
        val expected = 300
        assertEquals(expected, totalScore(rolls))
    }

}